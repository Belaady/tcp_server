from socket import *
import string
import random

serverPort = 12000
serverSocket = socket(AF_INET,SOCK_STREAM) 
serverSocket.bind(('',serverPort)) 
serverSocket.listen(1)
print('The server is ready to receive') 
session = {}

def login(param):
    client = param[1]
    idSession = "".join(random.choices(string.ascii_letters + string.digits, k = 6))
    session[idSession] = client
    return idSession

def message_sending(param):
  idSession = param[1]
  message = " ".join(param[2:])
  clientName  = session.get(idSession)
  if clientName:
    return "ANSWER {} {}".format(clientName, message)
  return "ANSWER You are not logged in"


while(True):
    connectionSocket, addr = serverSocket.accept()
    sentence = connectionSocket.recv(1024)
    loweredSentence = sentence.lower()  
    param = sentence.decode().split()
    print("Request received from client : ", loweredSentence)
    
    if len(param) > 1 and param[0].lower() == "login":
        idSession = login(param)
        connectionSocket.send("NEW_SESSION {}".format(idSession).encode())
    elif len(param) > 2 and param[0].lower() == "msg":
        connectionSocket.send(message_sending(param).encode())
    else:
        connectionSocket.send("Wrong Command! Try another".encode())
    print("")